## SpringBoot + mica-mqtt-server 应用演示

## 启动步骤
1. 先启动 mica-mqtt-client-spring-boot-example

2. 启动 mica-mqtt-client-spring-boot-example

4. 查看控制器 swagger 地址：http://localhost:30012/doc.html

5. 可开启 prometheus 指标收集，详见： http://localhost:30012/actuator/prometheus

## 连接

mica Spring boot 开发组件集文档：https://www.dreamlu.net/components/mica-swagger.html
